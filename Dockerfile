FROM alpine:3.14

RUN apk add --no-cache --virtual .build-deps \
        binutils \
    && apk add --no-cache curl \
    && GLIBC_VER=$(curl -s https://api.github.com/repos/sgerrand/alpine-pkg-glibc/releases/latest | grep tag_name | cut -d : -f 2,3 | tr -d \",' ') \
    && curl -sL https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub -o /etc/apk/keys/sgerrand.rsa.pub \
    && curl -sLO https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VER}/glibc-${GLIBC_VER}.apk \
    && curl -sLO https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VER}/glibc-bin-${GLIBC_VER}.apk \
    && apk update && apk add --no-cache \
        glibc-${GLIBC_VER}.apk \
        glibc-bin-${GLIBC_VER}.apk \
        bash \
        coreutils \
    # Install kubectl
    && curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" \
    && install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl \
    # Install AWS_CLI_v2
    && curl -sL https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o awscliv2.zip \
    && unzip awscliv2.zip \
    && aws/install \
    # Install SAM CLI
    && curl -sL https://github.com/aws/aws-sam-cli/releases/latest/download/aws-sam-cli-linux-x86_64.zip -o awssamcli.zip \
    && unzip awssamcli.zip -d awssamcli \
    && awssamcli/install \
    # Install dotnet sdk dependencies for Alpine
    && apk add icu-libs krb5-libs libgcc libintl libssl1.1 libstdc++ zlib \
    # if the .NET app requires the System.Drawing.Common assembly
    # && apk add libgdiplus --repository https://dl-3.alpinelinux.org/alpine/edge/testing/ \
    && curl -O https://download.visualstudio.microsoft.com/download/pr/bb523fba-7eb0-49ff-8214-c78c65dae090/7e7f9798ee57bf93649ada3eb13a79ae/dotnet-sdk-6.0.100-linux-musl-x64.tar.gz \
    && mkdir -p /usr/bin/dotnet \
    && tar -xzvf dotnet-sdk-6.0.100-linux-musl-x64.tar.gz -C /usr/bin/dotnet \
    && apk del --no-cache .build-deps \
    && rm -rf \
        awscliv2.zip \
        /usr/local/aws-cli/v2/*/dist/aws_completer \
        /usr/local/aws-cli/v2/*/dist/awscli/data/ac.index \
        /usr/local/aws-cli/v2/*/dist/awscli/examples \
        awssamcli.zip \
        dotnet-sdk-5.0.401-linux-musl-x64.tar.gz \
        aws* \
        glibc* \
        /tmp/* \
        /var/cache/apk/*

# Install docker cli
RUN apk add --no-cache --update docker openrc
RUN rc-update add docker boot

# Export dotnet command
ENV PATH=$PATH:/usr/bin/dotnet
